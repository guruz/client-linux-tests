#!/usr/bin/env bash
set -e

yum install -y wget
wget https://dl.fedoraproject.org/pub/epel/epel-release-latest-$1.noarch.rpm
yum install -y epel-release-latest-$1.noarch.rpm
yum install -y python34
