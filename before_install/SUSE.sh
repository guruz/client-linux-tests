#!/usr/bin/env bash
set -e

zypper addrepo $REPOSITORY
zypper --gpg-auto-import-keys refresh