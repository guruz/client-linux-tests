#!/usr/bin/env bash
set -e

#  Check for messages that indicate an error to load openSSL at runtime 
#   Debian 9 comes with libssl-1.1, currently unsupported by Qt5.6
#   see err.ref. https://gitlab.com/alfageme/client-linux-tests/-/jobs/28494707 

if [ $($APPLICATION_EXECUTABLE'cmd' --version | grep QSslSocket | wc -l) -ne 0 ];
then
    echo -e "❌\033[0;31m - An error ocurred loading openSSL.\033[0m"
    exit 1
else 
    echo -e "✅\033[0;32m - OpenSSL was loaded correctly at runtime.\033[0m"
fi