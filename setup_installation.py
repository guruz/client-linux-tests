#!/usr/bin/env python3

'''
 Script to set up the package manager to install owncloud-client.
'''
import os
import subprocess
import urllib.request
from urllib.parse import urlparse, parse_qs
import json
from optparse import OptionParser

def get_repo_url(url,distribution,version):
	response = urllib.request.urlopen(url)
	data = json.loads(response.read().decode("utf-8"))

	# FIXME: find how this is configured/if there's a way to get it included on the json/peek at the directory to match *.repo
	subfix = parse_qs(urlparse(url).query)["project"][0] + '.repo'

	tag = distribution+"_"+version

	# workaround for https://github.com/openSUSE/software-o-o/issues/123
	data[tag]['repo'] = data[tag]['repo'].replace('https://', 'http://', 1)

	if ('Ubuntu' in distribution or distribution == 'Debian'):
		return data[tag]['repo']
	else:
		return data[tag]['repo']+subfix

if __name__ == '__main__':

	parser = OptionParser()

	parser.add_option("-u", "--url",
	                    help="Specify the repository json description.")
	parser.add_option("-d", "--distribution",
	                    help="Specify the Linux distribution.")
	parser.add_option("-v", "--version",
	                    help="Specify the distribution version.")
	parser.add_option("-o", "--override",
						help="Override the repository descriptor with an URL.")
	parser.add_option("-k", "--key",
						help="Override the repository key default URL.")

	(options, args) = parser.parse_args()

	url 		 = options.url
	distribution = options.distribution
	version 	 = options.version
	override_url = options.override
	key_url 	 = options.key

	if override_url is None:
		target_url = get_repo_url(url,distribution,version)
		os.environ["REPOSITORY"] = target_url
		# URL valid/used only for Debian/Ubuntu repositories
		os.environ["REPOSITORY_KEY"] = target_url+'Release.key'
	else:
		os.environ["REPOSITORY"] = override_url
	
	if key_url is not None:
		os.environ["REPOSITORY_KEY"] = key_url

	# Workaround for SUSE.sh
	distribution = distribution if not "SUSE" in distribution else "SUSE"
	# ... the same for the xUbuntu notation
	distribution = distribution if not "Ubuntu" in distribution else "Ubuntu"

	subprocess.call('before_install/'+distribution+'.sh')
